# 123 Planner
A milestone manager developed as a project for Web Platform Development 2 @ Glasgow Caledonian University 2019


#Built using
Maven - Dependancy Management
Jetty server - For portable web serving 
PostgreSQL - For remote database

#Authors
Joseph Degnan, Jan Krämer & Bereketab Gulai 

