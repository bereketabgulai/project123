package Planner.Servlets;

import lombok.NonNull;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.UUID;

public class UserHandler {

    final static String USERNAME_PARAMETER = "userName";
    final static String PASSWORD_PARAMETER = "userPassword";
    final static String FULLNAME_PARAMETER = "fullName";
//    final static String PROJECT_PARAMETER = "currentProject";

    final static String DEFAULT_LOGIN_REDIRECT = "/home";
    private final static String USER_NAME_KEY = "userName";
    private final static String USER_ID_KEY = "userId";


    private final static String LOGIN_REDIRECT_KEY = "redirectURL";

    private UserHandler() {
    }

    /**
     * @param request
     * @param userName
     */
    static void setCurrentUser(HttpServletRequest request, @NonNull String userName, @NonNull String fullName, @NonNull UUID userId) {
        HttpSession session = request.getSession(true);
        session.setAttribute(USER_NAME_KEY, userName);
        session.setAttribute(USER_ID_KEY, userId);
        session.setAttribute(FULLNAME_PARAMETER, fullName);
    }

    /**
     * @param request
     */
    static void clearCurrentUser(HttpServletRequest request) {
        HttpSession session = request.getSession(true);
        session.removeAttribute(USER_NAME_KEY);
        session.removeAttribute(FULLNAME_PARAMETER);
    }

    /**
     * Find the current user's username, if any
     *
     * @param request The HTTP request object, containing the session, if any
     * @return The current user, or the empty string if none (note NOT null)
     */
    static String getCurrentUserName(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return "";
        }
        String val = (String) session.getAttribute(USER_NAME_KEY);
        return val == null ? "" : val;
    }

    /**
     * Find the current user's id, if any
     *
     * @param request The HTTP request object, containing the session, if any
     * @return The current user, or the empty string if none (note NOT null)
     */
    static UUID getCurrentUserId(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return null;
        }
        UUID val = (UUID) session.getAttribute(USER_ID_KEY);
        return val;
    }

    /**
     * Find the current user's full name, if any
     *
     * @param request The HTTP request object, containing the session, if any
     * @return The current user, or the empty string if none (note NOT null)
     */
    static String getCurrentFullName(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return "";
        }
        String val = (String) session.getAttribute(FULLNAME_PARAMETER);
        return val == null ? "" : val;
    }

    /**
     * @param request
     * @return
     */
    static String getLoginRedirect(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return DEFAULT_LOGIN_REDIRECT;
        }
        String redirectURL = (String) session.getAttribute(LOGIN_REDIRECT_KEY);
        session.removeAttribute(LOGIN_REDIRECT_KEY);
        return (redirectURL == null) ? DEFAULT_LOGIN_REDIRECT : redirectURL;
    }

    /**
     * @param request
     */
    static void setLoginRedirect(HttpServletRequest request) {
        String uri = request.getRequestURI();
        HttpSession session = request.getSession(true);
        session.setAttribute(LOGIN_REDIRECT_KEY, uri);
    }
}
