// Copyright (c) 2018 Cilogi. All Rights Reserved.
//
// File:        LoginServlet.java
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//

package Planner.Servlets;

import Planner.Classes.Database.DataAccessObjects.ProjectDAO;
import Planner.Classes.Database.DataAccessObjects.SaltDAO;
import Planner.Classes.Database.DataAccessObjects.UserDAO;
import Planner.Classes.Password;
import Planner.Models.ProjectModel;
import Planner.Models.SaltModel;
import Planner.Models.UserModel;
import org.apache.commons.codec.binary.Hex;
import org.owasp.html.PolicyFactory;
import org.owasp.html.Sanitizers;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

public class RegisterServlet extends BaseServlet {

    private final String REGISTER_TEMPLATE = "/authentication/register.mustache";
    private final String DEFAULT_PROJECT_NAME = "Default";

    // For sanitizing inputs
    PolicyFactory policy = Sanitizers.FORMATTING.and(Sanitizers.LINKS);

    public RegisterServlet() {

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        showView(response, REGISTER_TEMPLATE, null);
        if ("delete".equals(request.getParameter("action"))) {
            doDelete(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String fullName = policy.sanitize(request.getParameter("fullName"));
        String username = policy.sanitize(request.getParameter("username"));
        String password = policy.sanitize(request.getParameter("userPassword"));

        if (!username.equals("") && !password.equals("")) {
            UserDAO userDAO = new UserDAO();
            SaltDAO saltDAO = new SaltDAO();
            UserModel user = new UserModel();
            SaltModel salt = new SaltModel();
            int userStatus = 0;
            int saltStatus = 0;
            String hash;

            try {
                user.setId(UUID.randomUUID());
                user.setUsername(username);
                user.setSalt(Hex.encodeHexString(Password.createSalt()));
                hash = Password.hashPassword(password, user.getSalt());
                user.setPassword(hash);
                user.setFullName(fullName);

                salt.setId(UUID.randomUUID());
                salt.setSalt(user.getSalt());
                salt.setUserId(user.getId());

                userStatus = userDAO.add(user);
                saltStatus = saltDAO.add(salt);
            } catch (Exception e) {
                e.printStackTrace();
            }

            // If register user successful
            if (userStatus == 1 && saltStatus == 1) {

                // Add default project
                ProjectDAO projectDAO = new ProjectDAO();
                ProjectModel projectModel = new ProjectModel();
                int projectStatus = 0;

                projectModel.setId(UUID.randomUUID());
                projectModel.setName(DEFAULT_PROJECT_NAME);

                try {
                    projectStatus = projectDAO.add(projectModel);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (projectStatus == 1) {
                    try {
                        projectDAO.addUser(projectModel.getId(), user.getId());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                UserHandler.setCurrentUser(request, username, fullName, user.getId());
                String targetURL = UserHandler.getLoginRedirect(request);
                response.sendRedirect(response.encodeRedirectURL(targetURL));
            } else {
                try {
                    userDAO.delete(user.getId());
                    saltDAO.delete(salt.getId());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
