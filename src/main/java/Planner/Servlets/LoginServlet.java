// Copyright (c) 2018 Cilogi. All Rights Reserved.
//
// File:        LoginServlet.java
//
// Copyright in the whole and every part of this source file belongs to
// Cilogi (the Author) and may not be used, sold, licenced, 
// transferred, copied or reproduced in whole or in part in 
// any manner or form or in or on any media to any person other than 
// in accordance with the terms of The Author's agreement
// or otherwise without the prior written consent of The Author.  All
// information contained in this source file is confidential information
// belonging to The Author and as such may not be disclosed other
// than in accordance with the terms of The Author's agreement, or
// otherwise, without the prior written consent of The Author.  As
// confidential information this source file must be kept fully and
// effectively secure at all times.
//

package Planner.Servlets;

import Planner.Classes.Database.DataAccessObjects.SaltDAO;
import Planner.Classes.Database.DataAccessObjects.UserDAO;
import Planner.Classes.Password;
import Planner.Models.SaltModel;
import Planner.Models.ToDoAppModel;
import Planner.Models.UserModel;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

public class LoginServlet extends BaseServlet {

    private static final long serialVersionUID = 2246502321286918143L;

    private final String LOGIN_TEMPLATE = "/authentication/login.mustache";

    public LoginServlet() {

    }

    /**
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userName = UserHandler.getCurrentUserName(request);


        // Compare with existing value, if parameter is null then no nullpointer error
        if (("delete").equals(request.getParameter("action"))) {
            doDelete(request, response);
        }

        showView(response, LOGIN_TEMPLATE, userName);
    }

    /**
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Optional<UserModel> userOptional = Optional.empty();
        Optional<SaltModel> saltOptional = Optional.empty();
        UserModel user = new UserModel();
        UserDAO userDAO = new UserDAO();
        SaltDAO saltDAO = new SaltDAO();

        ToDoAppModel toDoAppModel = new ToDoAppModel();

        String tempEncrypted = "";

        String name = request.getParameter(UserHandler.USERNAME_PARAMETER);
        String password = request.getParameter(UserHandler.PASSWORD_PARAMETER);


        if ((name != null && name.length() > 0) && (password != null && password.length() > 0)) {

            try {
                userOptional = userDAO.getByName(name);
            } catch (SQLException e) {
                e.printStackTrace();
            }

            try {
                if (userOptional.isPresent()) {
                    user = userOptional.get();
                    saltOptional = saltDAO.getByUser(user.getId());
                }
            } catch (Exception e) {
                e.getStackTrace();
            }

            if (saltOptional.isPresent()) {

                SaltModel salt = saltOptional.get();

                tempEncrypted = Password.hashPassword(password, salt.getSalt());

                if (tempEncrypted.equals(user.getPassword())) {

                    System.out.println("Access Granted.");

                    UserHandler.setCurrentUser(request, name, user.getFullName(), user.getId());
                    String targetURL = UserHandler.getLoginRedirect(request);
                    response.sendRedirect(response.encodeRedirectURL(targetURL));
                } else {
                    System.out.println("Access Denied.");
                    // TODO: make it ajax call to make further interactive
                    response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE, "Wrong username or password. Access denied.");
//                    response.getWriter().write(getErrorToast());

                }
            } else response.sendRedirect("/");
        }
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserHandler.clearCurrentUser(request);
        response.sendRedirect(response.encodeRedirectURL(UserHandler.DEFAULT_LOGIN_REDIRECT));
    }
}
