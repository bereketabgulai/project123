package Planner.Servlets;

import Planner.Classes.Database.DataAccessObjects.MilestoneDAO;
import Planner.Models.MilestoneModel;
import Planner.Models.ToDoAppModel;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

public class SharelinkServlet extends BaseServlet {

    private static final long serialVersionUID = -8988086341822702885L;

    private static final Logger LOG = LoggerFactory.getLogger(ControllerServlet.class);

    private static final String MILESTONE_VIEW = "ShareView.mustache";
    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("EE dd MMMM yyyy");
    ToDoAppModel toDoAppModel;
    private Map<String, Object> context;
    private MilestoneDAO milestoneDAO;

    public SharelinkServlet() {
        try {
            context = new HashMap<>();
            toDoAppModel = new ToDoAppModel();

            context.put("formattedDate", FORMAT.format(new Date()));
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    /**
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String msId = request.getParameter("id") != null ? request.getParameter("id") :
                request.getParameter("milestone-uuid") != null ? request.getParameter("milestone-uuid") : "";
        if (!msId.equals("")) {
            milestoneDAO = new MilestoneDAO();
            UUID milestoneUuid = UUID.fromString(msId);

            if (("true").equals(request.getParameter("get-milestone-share-uuid"))) {
                Map<String, String> responseMap = new HashMap<>();
                String uuid = "";
                try {
                    Optional<MilestoneModel> optMilestone = milestoneDAO.getShared(milestoneUuid);
                    if (optMilestone.isPresent()) {
                        MilestoneModel milestoneModel = optMilestone.get();
                        uuid = milestoneModel.getId().toString();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                // Check if it is already shared
                if (!uuid.equals("")) {
                    responseMap.put("shared", "true");
                    responseMap.put("sharedUuid", uuid);
                } else {
                    // Otherwise return random uuid
                    responseMap.put("shared", "false");
                    responseMap.put("sharedUuid", milestoneUuid.toString());
                }

                response.getWriter().write(new Gson().toJson(responseMap));
                // terminate
                return;
            }

            try {
                context.put("milestone", (milestoneDAO.getShared(milestoneUuid).isPresent()) ?
                        milestoneDAO.getShared(milestoneUuid).get() : null);
            } catch (SQLException e) {
                e.printStackTrace();
            }

            showView(response, MILESTONE_VIEW, context);
            System.out.println(context.get("milestone"));
        } else {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Provide Id");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UUID milestoneUuid = UUID.fromString(request.getParameter("milestone-uuid"));
        UUID milestoneShareUuid = UUID.randomUUID();
        int result = 0;

        try {
            milestoneDAO = new MilestoneDAO();
            result = milestoneDAO.addShared(milestoneShareUuid, milestoneUuid);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (result == 1) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}
