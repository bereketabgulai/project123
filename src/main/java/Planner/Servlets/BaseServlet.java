package Planner.Servlets;

import Planner.Classes.MustacheRenderer;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class BaseServlet extends HttpServlet {

    public static final String PLAIN_TEXT_UTF_8 = "text/plain; charset=UTF-8";
    public static final String HTML_UTF_8 = "text/html; charset=UTF-8";
    public static final Charset CHARSET_UTF8 = Charset.forName("UTF-8");
    static final Set<String> PROTECTED_PAGES = new HashSet<>(Arrays.asList("/home"));
    static final String LOGIN_PAGE = "/login";
    private static final long serialVersionUID = 1L;
    private final MustacheRenderer mustache;

    protected BaseServlet() {
        mustache = new MustacheRenderer();
    }

    protected void issue(String mimeType, int returnCode, byte[] output, HttpServletResponse response)
            throws IOException {
        response.setContentType(mimeType);
        response.setStatus(returnCode);
        response.getOutputStream().write(output);
    }

    protected void cache(HttpServletResponse response, int seconds) {
        if (seconds > 0) {
            response.setHeader("Pragma", "Public");
            response.setHeader("Cache-Control", "public, no-transform, max-age=" + seconds);
        }
    }

    void showView(HttpServletResponse response, String templateName, Object model) throws IOException {
        String html = mustache.render(templateName, model);
        issue(HTML_UTF_8, HttpServletResponse.SC_OK, html.getBytes(CHARSET_UTF8), response);
    }

    /**
     * @param request
     * @param response
     * @return
     * @throws IOException
     */
    boolean authOK(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String uri = request.getRequestURI();
        String userName = UserHandler.getCurrentUserName(request);
        if (PROTECTED_PAGES.contains(uri) && "".equals(userName)) {
            UserHandler.setLoginRedirect(request);
            response.sendRedirect(response.encodeRedirectURL(LOGIN_PAGE));
            return false;
        }
        return true;
    }
}
