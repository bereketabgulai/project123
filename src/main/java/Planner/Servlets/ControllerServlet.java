package Planner.Servlets;

import Planner.Classes.Database.DataAccessObjects.MilestoneDAO;
import Planner.Classes.Database.DataAccessObjects.ProjectDAO;
import Planner.Models.MilestoneModel;
import Planner.Models.ToDoAppModel;
import Planner.Models.UserModel;
import com.google.gson.Gson;
import org.owasp.html.PolicyFactory;
import org.owasp.html.Sanitizers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;


public class ControllerServlet extends BaseServlet {

    private static final long serialVersionUID = 687117339002032958L;

    private static final Logger LOG = LoggerFactory.getLogger(ControllerServlet.class);

    private static final String MILESTONE_VIEW = "MilestoneView.mustache";
    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("EE dd MMMM yyyy");
    private Map<String, Object> context;
    private ToDoAppModel toDoAppModel;
    private UUID currentMilestoneId;

    // For sanitizing inputs
    PolicyFactory policy = Sanitizers.FORMATTING.and(Sanitizers.LINKS);


    public ControllerServlet() {
        try {
            context = new HashMap<>();
            toDoAppModel = new ToDoAppModel();

            context.put("formattedDate", FORMAT.format(new Date()));

        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (!authOK(request, response)) {
            return;
        }

        // When the request is from edit button, indicated by the parameter, do this
        if (("true").equals(request.getParameter("get-milestone-data"))) {
            UUID milestoneUuid = UUID.fromString(request.getParameter("milestone-uuid"));
            String milestoneJsonString = new Gson().toJson(getMilestoneData(milestoneUuid));
            LOG.info(milestoneJsonString);
            response.getWriter().write(milestoneJsonString);
            return;
        }

        // Main task
        UUID userId = UserHandler.getCurrentUserId(request);
        String fullName = UserHandler.getCurrentFullName(request);
        try {
            List<List<MilestoneModel>> milestonesLists = getCompAndInCompMilestones(getUserMilestones(userId));

            context.put("fullName", fullName);
            context.put("projects", (new ProjectDAO().getByUser(userId)));
            context.put("milestonesIncomplete", milestonesLists.get(0));
            context.put("milestonesComplete", milestonesLists.get(1));
        } catch (
                SQLException e) {
            e.printStackTrace();
        }

        showView(response, MILESTONE_VIEW, context);
    }

    /**
     * @param milestoneUuid
     * @return
     */
    private MilestoneModel getMilestoneData(UUID milestoneUuid) {
        MilestoneModel milestoneModel = new MilestoneModel();

        try {
            milestoneModel = toDoAppModel.getMilestone(milestoneUuid);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return milestoneModel;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {



        String name = request.getParameter("milestone-name");
        String dateDue = request.getParameter("dateDue");
        UUID userId = UserHandler.getCurrentUserId(request);

        if ((!name.equals("") && !dateDue.equals(""))) {
            Date dateToFinishBy = new Date();

            try {
                dateToFinishBy = new SimpleDateFormat("yyyy-MM-dd").parse(dateDue);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (addMilestone(name, dateToFinishBy, userId)) {
                response.setContentType("text/html");
                response.setCharacterEncoding("UTF-8");

                // Format date
                dateDue = formatDisplayDate(dateToFinishBy);
                // Respond with card details
                response.getWriter().write(getMilestoneAddContent(policy.sanitize(name), dateDue, currentMilestoneId.toString()));
            }
        }
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        MilestoneDAO milestoneDAO = new MilestoneDAO();
        UUID milestoneId = null;
        int result = 0;

        try {
            milestoneId = UUID.fromString(request.getParameter("milestoneId"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            result = milestoneDAO.delete(milestoneId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        if (result == 1) {
            try {
                response.setStatus(HttpServletResponse.SC_OK);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            response.setStatus(HttpServletResponse.SC_ACCEPTED);
        }
    }

    /**
     * Handles doComplete, doEdit
     *
     * @param request  HttpServletRequest
     * @param response HttpServletResponse
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        MilestoneModel milestoneModel = new MilestoneModel();
        ToDoAppModel toDoAppModel = new ToDoAppModel();
        String responseContent = "";

        String fromComplete = request.getParameter("fromComplete");
        String name = policy.sanitize(request.getParameter("milestone-name"));
        String dateDue = request.getParameter("dateDue");
        String dateCompleted = ((dateCompleted = request.getParameter("dateCompleted")) != null) ? dateCompleted : "";
        UUID uuid = UUID.fromString(request.getParameter("milestone-uuid"));


        System.out.println("name = " + name + " dateDue = " + dateDue);

        try {
            milestoneModel = toDoAppModel.getMilestone(uuid);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            Date tmpDateDue = format.parse(dateDue);

            milestoneModel.setDueDate(tmpDateDue);
            milestoneModel.setName(name);

            if (!("").equals(dateCompleted)) {
                Date tmpDateCompleted = new Date(Long.parseLong(dateCompleted, 10));
                milestoneModel.setDateCompleted(tmpDateCompleted);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            toDoAppModel.updateMilestone(milestoneModel);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // TODO: check if update was successful
        if (("true").equals(fromComplete)) {
            try {
                if ((new MilestoneDAO().getShared(milestoneModel.getId()).isPresent())) {
                    milestoneModel.setShared(true);
                } else {
                    milestoneModel.setShared(false);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            responseContent = getMilestoneCompleteContent(milestoneModel.getName(), formatDisplayDate(milestoneModel.getDueDate()),
                    formatDisplayDate(milestoneModel.getDateCompleted()), milestoneModel.getId().toString(), milestoneModel.isShared());
        } else {
            responseContent = new Gson().toJson(milestoneModel);
        }

        try {
            response.setContentType("text/html");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(responseContent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets a single user milestone
     *
     * @param username for user
     * @return single milestone
     * @throws SQLException
     */
    private MilestoneModel getUserMilestone(String username) throws SQLException {
        List<UserModel> users = toDoAppModel.getUsers();

        UUID uuid = UUID.randomUUID();

        for (UserModel user : users) {

            if (username.equals(user.getUsername())) {
                uuid = user.getId();
            }
        }
        return toDoAppModel.getMilestone(uuid);
    }

    /**
     * Gets milestones for the given userid
     *
     * @param userId for use in this class, for the current user
     * @return ArrayList<MilestoneModel> list of models
     * @throws SQLException watch out
     */
    private List<MilestoneModel> getUserMilestones(UUID userId) throws SQLException {
        // TODO: change to get for specific project
        return new MilestoneDAO().getByUser(userId);
    }


    private Optional<MilestoneModel> getAll() throws SQLException {

        Optional opt = toDoAppModel.getMilestones();
        return opt;
    }

    /**
     * @param name
     * @param dateDue
     * @param userId
     * @return
     */
    private boolean addMilestone(String name, Date dateDue, UUID userId) {
        MilestoneDAO milestoneDAO = new MilestoneDAO();
        MilestoneModel milestoneModel = new MilestoneModel();
        Date dateFinished = new Date(0);
        boolean success = false;

        try {
            currentMilestoneId = UUID.randomUUID();
            milestoneModel.setId(currentMilestoneId);
            milestoneModel.setName(name);
            milestoneModel.setDueDate(dateDue);
            milestoneModel.setDateCompleted(dateFinished);
            milestoneModel.setProjectId((new ProjectDAO().getByUser(userId)).get(0).getId());
            milestoneModel.setUserId(userId);

            // If milestone user successful
            if (milestoneDAO.add(milestoneModel) == 1) {
                System.out.println("Success add Milestone");
                success = true;
            } else {
                System.out.println("Failure add Milestone");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success;
    }

    private List<List<MilestoneModel>> getCompAndInCompMilestones(List<MilestoneModel> milestones) throws SQLException {
        List<MilestoneModel> milestoneModelListIncomplete = new ArrayList<>();
        List<MilestoneModel> milestoneModelListComplete = new ArrayList<>();
        List<List<MilestoneModel>> milestoneModelList = new ArrayList<>();
        MilestoneDAO milestoneDAO = new MilestoneDAO();

        for (MilestoneModel milestoneModel : milestones) {
            LocalDateTime localDateTime = LocalDateTime.ofInstant(milestoneModel.getDateCompleted().toInstant(), ZoneId.systemDefault());
            if (localDateTime.getYear() != 1970) {
                milestoneModelListComplete.add(milestoneModel);
            } else {
                milestoneModelListIncomplete.add(milestoneModel);
            }

            // Check shared
            if (milestoneDAO.getShared(milestoneModel.getId()).isPresent()) {
                milestoneModel.setShared(true);
            }
        }

        milestoneModelList.add(milestoneModelListIncomplete);
        milestoneModelList.add(milestoneModelListComplete);

        return milestoneModelList;
    }

    /**
     * Returns content for added milestone
     *
     * @param name        string
     * @param dateDue     string
     * @param milestoneId string
     * @return string
     */
    private String getMilestoneAddContent(String name, String dateDue, String milestoneId) {
        return "<div class=\"row bg-light border rounded m-1 mt-2\" id=\"milestone-" + milestoneId + "\" style=\"display: none;\">\n" +
                "                    <div class=\"col-6\">\n" +
                "                        <label id=\"m-title-" + milestoneId + "\" class=\"milestone-title\">" + name + "</label>" +
                "                        <p id=\"m-date-" + milestoneId + "\">" + dateDue + "</p>\n" +
                "                    </div>\n" +
                "                    <div class=\"col-6 justify-content-end\">\n" +
                "                        <div class=\"btn-group btn-block pt-1\" role=\"group\" aria-label=\"MS controls\">\n" +
                "                            <button id=\"" + milestoneId + "_edit\" type=\"button\" class=\"btn milestoneBtn\"><i class=\"fas fa-edit fa-lg\"></i></button>\n" +
                "                            <button id=\"" + milestoneId + "_trash\" type=\"button\" class=\"btn milestoneBtn\"><i class=\"fas fa-trash-alt fa-lg\"></i></button>\n" +
                "                            <button id=\"" + milestoneId + "_share\" type=\"button\" class=\"btn milestoneBtn\"><i class=\"far fa-share-square fa-lg\"></i></button>\n" +
                "                            <button id=\"" + milestoneId + "_comp\" type=\"button\" class=\"btn milestoneBtn\"><i class=\"far fa-check-circle fa-lg\"></i></button>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "                </div>";

    }

    /**
     * Returns content for completed milestone
     *
     * @param name        string
     * @param dateDue     string
     * @param milestoneId string
     * @return string
     */
    private String getMilestoneCompleteContent(String name, String dateDue,  String completedDate,String milestoneId, boolean shared) {
        String shareIcon = "<i class=\"far fa-share-square fa-lg\"></i>";
        if (shared) {
            shareIcon = "<i class=\"fas fa-share-square fa-lg\"></i>";
        }

        return "<div class=\"row bg-light border rounded m-1 mt-2\" id=\"milestone-" + milestoneId + "\">\n" +
                "                    <div class=\"col-6\">\n" +
                "                        <label id=\"m-title-" + milestoneId + "\" class=\"milestone-title\">" + name + "</label>" +
                "                        <p id=\"m-date-" + milestoneId + "\">" + dateDue + "</p>\n" +
                "                    </div>\n" +
                "                    <div class=\"col-6 justify-content-end\">\n" +
                "                        <div class=\"btn-group btn-block pt-1\" role=\"group\" aria-label=\"MS controls\">\n" +
                "                            <button type=\"button\" class=\"btn milestoneBtn disabled\" disabled><i class=\"fas fa-edit fa-lg\"></i></button>\n" +
                "                            <button id=\"" + milestoneId + "_trash\" type=\"button\" class=\"btn milestoneBtn\"><i class=\"fas fa-trash-alt fa-lg\"></i></button>\n" +
                "                            <button id=\"" + milestoneId + "_share\" type=\"button\" class=\"btn milestoneBtn\">" + shareIcon + "</button>\n" +
                "                            <button  type=\"button\" class=\"btn milestoneBtn disabled\" disabled><i class=\"fas fa-check-circle fa-lg\"></i></button>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "<label class=\"text-muted ml-5\">Completed on <p style=\"display: inline;\">" + completedDate + "</p></label>" +
                "                </div>";

    }

    /**
     * Formats milestone display date
     *
     * @param date date type
     * @return string
     */
    private String formatDisplayDate(Date date) {
        return new SimpleDateFormat("EE, dd/MM/yyyy").format((date));
    }
}
