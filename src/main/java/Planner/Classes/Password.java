package Planner.Classes;

import org.apache.commons.codec.binary.Hex;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

public class Password {

    //
    public static String hashPassword(String password, String salt) {

        byte[] hash = null;
        byte[] tmpSalt = null;

        try {
            tmpSalt = Hex.decodeHex(salt.toCharArray());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            hash = hash(password, tmpSalt);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Hex.encodeHexString(hash);
    }

    private static byte[] hash(String password, byte[] salt) {

        byte[] hash = null;

        try {
            PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 512);
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");

            hash = factory.generateSecret(spec).getEncoded();
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            System.err.println(e.getMessage());
        }

        return hash;
    }

    public static byte[] createSalt() {

        byte[] salt = new byte[64];

        try {
            SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
            secureRandom.nextBytes(salt);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return salt;
    }
}
