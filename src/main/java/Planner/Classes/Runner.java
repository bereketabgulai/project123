package Planner.Classes;

import Planner.Servlets.ControllerServlet;
import Planner.Servlets.LoginServlet;
import Planner.Servlets.RegisterServlet;
import Planner.Servlets.SharelinkServlet;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Starting class to run full server
 */
public class Runner {
    private static final Logger LOG = LoggerFactory.getLogger(Runner.class);
    // private fields
//    private static final int PORT = 8080;

    /**
     * Constructor
     */
    private Runner() {

    }

    /**
     * Main
     *
     * @param args command line arguments (none implemented)
     */
    public static void main(String[] args) {
        try {
            LOG.info("starting...");
            new Runner().start();
        } catch (Exception e) {

            System.err.println("ERROR! Running Server: " + e.getMessage());
        }
    }

    /**
     * Sets up server
     *
     * @throws Exception
     */
    private void start() throws Exception {
        //The port that we should run on can be set into an environment variable
        //Look for that variable and default to 8080 if it isn't there.
        String webPort = System.getenv("PORT");
        if (webPort == null || webPort.isEmpty()) {
            webPort = "8080";
        }

        Server server = new Server(Integer.valueOf(webPort));

        ServletContextHandler handler = new ServletContextHandler(server, "/", ServletContextHandler.SESSIONS);

        handler.setContextPath("/");

        handler.setInitParameter("org.eclipse.jetty.servlet.Default." + "resourceBase", "src/main/resources/webapp");

        // Main controller servlet for milestones
        ControllerServlet controllerServlet = new ControllerServlet();
        handler.addServlet(new ServletHolder(controllerServlet), "/home");

        // Required to share Milestone to a person not in group
        SharelinkServlet sharelinkServlet = new SharelinkServlet();
        handler.addServlet(new ServletHolder(sharelinkServlet), "/share");

        // Handles user registration
        LoginServlet loginServlet = new LoginServlet();
        handler.addServlet(new ServletHolder(loginServlet), "/login");

        // Handles user registration
        RegisterServlet registerServlet = new RegisterServlet();
        handler.addServlet(new ServletHolder(registerServlet), "/register");

        // Default server to provide resources access
        DefaultServlet ds = new DefaultServlet();
        handler.addServlet(new ServletHolder(ds), "/");

        server.start();
        server.join();
    }
}
