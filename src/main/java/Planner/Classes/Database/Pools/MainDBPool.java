package Planner.Classes.Database.Pools;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class MainDBPool extends DBPool {

    private static HikariDataSource hikariDataSource;
    private static HikariConfig hikariConfig = new HikariConfig();

    static {
        try {
            hikariConfig.setJdbcUrl("jdbc:postgresql://ec2-79-125-4-72.eu-west-1.compute.amazonaws.com:5432/d35hi5u7rnue7f?sslmode=require&sslfactory=org.postgresql.ssl.NonValidatingFactory");
            hikariConfig.setDriverClassName("org.postgresql.Driver");
            hikariConfig.setUsername("mjzfemsmcelzds");
            hikariConfig.setPassword("72c079de46cb2aa1e3f10f7cf4621abffbae14cd5c5a86252fc851e62f4fbb55");
            hikariConfig.setMaximumPoolSize(5);
            hikariConfig.setMinimumIdle(2);
            hikariConfig.setIdleTimeout(60000);

            hikariConfig.addDataSourceProperty("cachePrepStmts", "true");
            hikariConfig.addDataSourceProperty("prepStmtCacheSize", "250");
            hikariConfig.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");

            hikariDataSource = new HikariDataSource(hikariConfig);
        } catch (Exception e) {
            // handle the exception
            e.printStackTrace();
        }
    }

    private MainDBPool() {
    }

    public static Connection getConnection() throws SQLException {
        return hikariDataSource.getConnection();
    }
}
