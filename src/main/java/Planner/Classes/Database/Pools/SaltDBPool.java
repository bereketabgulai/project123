package Planner.Classes.Database.Pools;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class SaltDBPool extends DBPool {

    private static HikariDataSource hikariDataSource;
    private static HikariConfig hikariConfig = new HikariConfig();

    static {
        try {
            hikariConfig.setJdbcUrl("jdbc:postgresql://ec2-79-125-4-72.eu-west-1.compute.amazonaws.com:5432/dp3ededoc60kd?sslmode=require&sslfactory=org.postgresql.ssl.NonValidatingFactory");
            hikariConfig.setDriverClassName("org.postgresql.Driver");
            hikariConfig.setUsername("tstjguwzmsbuic");
            hikariConfig.setPassword("47da68b7ca1f6e1e0f43a68596f2a1f8ad7ca71f6ecfd74b47d84a6d7b1a56e9");
            hikariConfig.setMaximumPoolSize(5);
            hikariConfig.setMinimumIdle(2);
            hikariConfig.setIdleTimeout(60000);

            hikariConfig.addDataSourceProperty("cachePrepStmts", "true");
            hikariConfig.addDataSourceProperty("prepStmtCacheSize", "250");
            hikariConfig.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");

            hikariDataSource = new HikariDataSource(hikariConfig);
        } catch (Exception e) {
            // handle the exception
            e.printStackTrace();
        }
    }

    private SaltDBPool() {
    }

    public static Connection getConnection() throws SQLException {
        return hikariDataSource.getConnection();
    }
}
