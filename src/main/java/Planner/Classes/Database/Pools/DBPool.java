package Planner.Classes.Database.Pools;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public abstract class DBPool {

    protected static HikariDataSource hikariDataSource;
    protected static HikariConfig hikariConfig;

    /**
     * Get a connection to database
     *
     * @return Connection to a database is returned
     * @throws SQLException when a error during the connection initialization occurred
     */
    public static Connection getConnection() throws SQLException {
        return hikariDataSource.getConnection();
    }
}
