package Planner.Classes.Database;

import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class CommandDB {

    // Query data from the the database
    public static CachedRowSet select(String querySQL, Connection connection) throws SQLException {

        CachedRowSet cachedRowSet;

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(querySQL);

        RowSetFactory rowSetFactory = RowSetProvider.newFactory();
        cachedRowSet = rowSetFactory.createCachedRowSet();

        cachedRowSet.populate(resultSet);


        return cachedRowSet;
    }

    // Manipulate data in the database
    public static int manipulate(String commandSQL, Connection connection) throws SQLException {

        int statusCode;

        Statement statement = connection.createStatement();
        statusCode = statement.executeUpdate(commandSQL);

        return statusCode;
    }
}
