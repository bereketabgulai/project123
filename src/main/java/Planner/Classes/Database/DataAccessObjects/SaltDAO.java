package Planner.Classes.Database.DataAccessObjects;

import Planner.Classes.Database.CommandDB;
import Planner.Classes.Database.Pools.SaltDBPool;
import Planner.Models.SaltModel;

import javax.sql.rowset.CachedRowSet;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class SaltDAO extends DAO<SaltModel> {
    private int status;

    // CRUD Methods
    //region
    @Override
    public Optional<SaltModel> get(UUID saltId) throws SQLException {

        CachedRowSet cachedRowSet;

        try (Connection connection = SaltDBPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT id, salt, user_id " +
                            "FROM salts " +
                            "WHERE id=?");
            preparedStatement.setString(1, saltId.toString());

            cachedRowSet = CommandDB.select(getSQL(preparedStatement), connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return rowSetToModel(cachedRowSet);
    }

    @Override
    public List<SaltModel> getAll() throws SQLException {

        CachedRowSet cachedRowSet;

        try (Connection connection = SaltDBPool.getConnection()) {
            cachedRowSet = CommandDB.select(
                    "SELECT id, salt, user_id " +
                            "FROM salts", connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return rowSetToList(cachedRowSet);
    }

    @Override
    public int add(SaltModel salt) throws SQLException {

        try (Connection connection = SaltDBPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO salts " +
                            "(id, salt, user_id) " +
                            "VALUES (?, ?, ?)");
            preparedStatement.setString(1, salt.getId().toString());
            preparedStatement.setString(2, salt.getSalt());
            preparedStatement.setString(3, salt.getUserId().toString());

            status = CommandDB.manipulate(getSQL(preparedStatement), connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return status;
    }

    @Override
    public int update(SaltModel salt) throws SQLException {

        try (Connection connection = SaltDBPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "UPDATE salts " +
                            "SET salt=?, user_id=? " +
                            "WHERE id=?");
            preparedStatement.setString(1, salt.getSalt());
            preparedStatement.setString(2, salt.getUserId().toString());
            preparedStatement.setString(3, salt.getId().toString());

            status = CommandDB.manipulate(getSQL(preparedStatement), connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return status;
    }

    @Override
    public int delete(UUID saltId) throws SQLException {

        try (Connection connection = SaltDBPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "DELETE FROM salts " +
                            "WHERE id=?");
            preparedStatement.setString(1, saltId.toString());

            status = CommandDB.manipulate(getSQL(preparedStatement), connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return status;
    }
    //endregion

    // Additional Methods
    //region
    public Optional<SaltModel> getByUser(UUID userId) throws SQLException {

        CachedRowSet cachedRowSet;

        try (Connection connection = SaltDBPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT id, salt, user_id " +
                            "FROM salts " +
                            "WHERE user_id=?");
            preparedStatement.setString(1, userId.toString());

            cachedRowSet = CommandDB.select(getSQL(preparedStatement), connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return rowSetToModel(cachedRowSet);
    }
    //endregion

    // Helper
    //region
    @Override
    protected SaltModel mapToObject(CachedRowSet cachedRowSet) throws SQLException {

        SaltModel salt = new SaltModel();

        salt.setId(UUID.fromString(cachedRowSet.getString("id")));
        salt.setSalt(cachedRowSet.getString("salt"));
        salt.setUserId(UUID.fromString(cachedRowSet.getString("user_id")));

        return salt;
    }
    //endregion
}
