package Planner.Classes.Database.DataAccessObjects;

import Planner.Classes.Database.CommandDB;
import Planner.Classes.Database.Pools.MainDBPool;
import Planner.Models.MilestoneModel;

import javax.sql.rowset.CachedRowSet;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class MilestoneDAO extends DAO<MilestoneModel> {
    private int status;

    // CRUD Methods
    //region
    @Override
    public Optional<MilestoneModel> get(UUID milestoneId) throws SQLException {

        CachedRowSet cachedRowSet;

        try (Connection connection = MainDBPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT id, name, description, date_due, date_completed, project_id, user_id " +
                            "FROM milestones " +
                            "WHERE id=?");
            preparedStatement.setString(1, milestoneId.toString());

            cachedRowSet = CommandDB.select(getSQL(preparedStatement), connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return rowSetToModel(cachedRowSet);
    }

    @Override
    public List<MilestoneModel> getAll() throws SQLException {

        CachedRowSet cachedRowSet;

        try (Connection connection = MainDBPool.getConnection()) {
            cachedRowSet = CommandDB.select(
                    "SELECT id, name, description, date_due, date_completed, project_id, user_id " +
                            "FROM milestones",
                    connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return rowSetToList(cachedRowSet);
    }

    @Override
    public int add(MilestoneModel milestone) throws SQLException {

        try (Connection connection = MainDBPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO milestones " +
                            "(id, name, description, date_due, date_completed, user_id, project_id) " +
                            "VALUES (?, ?, ?, ?, ?, ?, ?)");
            preparedStatement.setString(1, milestone.getId().toString());
            preparedStatement.setString(2, milestone.getName());
            preparedStatement.setString(3, milestone.getDescription());
            preparedStatement.setTimestamp(4, new java.sql.Timestamp(milestone.getDueDate().getTime()));
            preparedStatement.setTimestamp(5, new java.sql.Timestamp(milestone.getDateCompleted().getTime()));
            preparedStatement.setString(6, milestone.getUserId().toString());
            preparedStatement.setString(7, milestone.getProjectId().toString());

            status = CommandDB.manipulate(getSQL(preparedStatement), connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return status;
    }

    @Override
    public int update(MilestoneModel milestone) throws SQLException {

        try (Connection connection = MainDBPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "UPDATE milestones " +
                            "SET name=?, description=?, date_due=?, date_completed=?, user_id=? " +
                            "WHERE id=?");
            preparedStatement.setString(1, milestone.getName());
            preparedStatement.setString(2, milestone.getDescription());
            preparedStatement.setTimestamp(3, new java.sql.Timestamp(milestone.getDueDate().getTime()));
            preparedStatement.setTimestamp(4, new java.sql.Timestamp(milestone.getDateCompleted().getTime()));
            preparedStatement.setString(5, milestone.getUserId().toString());
            preparedStatement.setString(6, milestone.getId().toString());

            status = CommandDB.manipulate(getSQL(preparedStatement), connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return status;
    }

    @Override
    public int delete(UUID milestoneId) throws SQLException {

        try (Connection connection = MainDBPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "DELETE FROM milestones " +
                            "WHERE id=?");
            preparedStatement.setString(1, milestoneId.toString());

            status = CommandDB.manipulate(getSQL(preparedStatement), connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return status;
    }
    //endregion

    // Additional Methods
    //region
    public List<MilestoneModel> getByProject(UUID projectId) throws SQLException {

        return getWhere("project_id", projectId);
    }

    public List<MilestoneModel> getByUser(UUID userId) throws SQLException {

        return getWhere("user_id", userId);
    }

    public Optional<MilestoneModel> getShared(UUID milestoneId) throws SQLException {
        CachedRowSet cachedRowSet;

        try (Connection connection = MainDBPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT milestones.id, milestones.name, milestones.description, milestones.date_due, " +
                            "milestones.date_completed, milestones.project_id, milestones.user_id " +
                            "FROM milestones JOIN share_links " +
                            "ON milestones.id = share_links.milestone_id " +
                            "WHERE share_links.milestone_id = ?");
            preparedStatement.setString(1, milestoneId.toString());

            cachedRowSet = CommandDB.select(getSQL(preparedStatement), connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return rowSetToModel(cachedRowSet);
    }

    public int addShared(UUID shareId, UUID milestoneId) throws SQLException {

        try (Connection connection = MainDBPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO share_links " +
                            "(id, milestone_id) " +
                            "VALUES (?, ?)");
            preparedStatement.setString(1, shareId.toString());
            preparedStatement.setString(2, milestoneId.toString());

            status = CommandDB.manipulate(getSQL(preparedStatement), connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return status;
    }

    public int deleteShared(UUID sharedUuid) throws SQLException {

        try (Connection connection = MainDBPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "DELETE FROM share_links " +
                            "WHERE milestone_id = ?");
            preparedStatement.setString(1, sharedUuid.toString());

            status = CommandDB.manipulate(getSQL(preparedStatement), connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return status;
    }
    //endregion

    // Helper
    //region
    private List<MilestoneModel> getWhere(String column, UUID id) throws SQLException {

        CachedRowSet cachedRowSet;

        try (Connection connection = MainDBPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    String.format("SELECT id, name, description, date_due, date_completed, project_id, user_id " +
                            "FROM milestones " +
                            "WHERE %s=?", column));
            preparedStatement.setString(1, id.toString());

            cachedRowSet = CommandDB.select(getSQL(preparedStatement), connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return rowSetToList(cachedRowSet);
    }

    @Override
    protected MilestoneModel mapToObject(CachedRowSet cachedRowSet) throws SQLException {

        MilestoneModel milestone = new MilestoneModel();

        milestone.setId(UUID.fromString(cachedRowSet.getString("id")));
        milestone.setName(cachedRowSet.getString("name"));
        milestone.setDescription(cachedRowSet.getString("description"));
        milestone.setDueDate(cachedRowSet.getTimestamp("date_due"));
        milestone.setDateCompleted(cachedRowSet.getTimestamp("date_completed"));
        milestone.setProjectId(UUID.fromString(cachedRowSet.getString("project_id")));
        milestone.setUserId(UUID.fromString(cachedRowSet.getString("user_id")));

        return milestone;
    }
    //endregion
}
