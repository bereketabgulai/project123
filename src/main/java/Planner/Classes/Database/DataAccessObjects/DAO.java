package Planner.Classes.Database.DataAccessObjects;

import javax.sql.rowset.CachedRowSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public abstract class DAO<T> {

    /**
     * Get a single entity by ID
     *
     * @return Optional<T> returns a optional with is empty or contains the model
     * @throws SQLException that occurred during database interaction
     */
    abstract Optional<T> get(UUID id) throws SQLException;

    /**
     * Get all entities
     *
     * @return List<T> is the queried list of objects
     * @throws SQLException that occurred during database interaction
     */
    abstract List<T> getAll() throws SQLException;

    /**
     * Add a single entity
     *
     * @param t is a valid Model for the DAO
     * @return int as status if get all the entities was successful
     * @throws SQLException that occurred during database interaction
     */
    abstract int add(T t) throws SQLException;

    /**
     * Update a single entity
     *
     * @param t is a valid Model for the DAO
     * @return int as status if adding the entity was successful
     * @throws SQLException that occurred during database interaction
     */
    abstract int update(T t) throws SQLException;

    /**
     * Delete single entity
     *
     * @param id is a valid Model for the DAO
     * @return int as status if delete the entity was successful
     * @throws SQLException that occurred during database interaction
     */
    abstract int delete(UUID id) throws SQLException;

    // Helper
    //region

    /**
     * Get the SQL statement from a prepared statement
     *
     * @param preparedStatement is a prepared statement that is already setup for execution
     * @return String which contains a SQL command
     * @throws SQLException thrown during extraction of the SQL command
     */
    protected String getSQL(PreparedStatement preparedStatement) throws SQLException {

        return preparedStatement.unwrap(PreparedStatement.class).toString();
    }

    /**
     * Loop trough a rowset and assign it to a list of objects
     *
     * @param cachedRowSet is a rowset with the queried data inside
     * @return List<T> which is a list of models that can be assigned from the rowset
     * @throws SQLException thrown during execution, for example asking for a not existing column
     */
    protected List<T> rowSetToList(CachedRowSet cachedRowSet) throws SQLException {

        List<T> list = new ArrayList<>();

        while (cachedRowSet.next()) {
            list.add(mapToObject(cachedRowSet));
        }

        return list;
    }

    /**
     * Get the first row from a Rowset and assign it to a object
     *
     * @param cachedRowSet is a rowset with the queried data inside
     * @return Optional<T> returns a optional with is empty or contains the model
     * @throws SQLException thrown during execution, for example asking for a not existing column
     */
    protected Optional<T> rowSetToModel(CachedRowSet cachedRowSet) throws SQLException {

        T model = null;

        if (cachedRowSet.first()) {
            model = mapToObject(cachedRowSet);
        }

        return Optional.ofNullable(model);
    }

    /**
     * Map a single row to a object
     *
     * @param cachedRowSet rowset with a cursor set to a row
     * @return T returns a model
     * @throws SQLException thrown when a not existing column is assigned
     */
    protected abstract T mapToObject(CachedRowSet cachedRowSet) throws SQLException;

    //endregion
}
