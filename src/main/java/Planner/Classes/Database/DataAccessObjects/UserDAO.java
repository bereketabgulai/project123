package Planner.Classes.Database.DataAccessObjects;

import Planner.Classes.Database.CommandDB;
import Planner.Classes.Database.Pools.MainDBPool;
import Planner.Models.UserModel;

import javax.sql.rowset.CachedRowSet;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class UserDAO extends DAO<UserModel> {
    private int status;

    // CRUD Methods
    //region
    @Override
    public Optional<UserModel> get(UUID userId) throws SQLException {

        CachedRowSet cachedRowSet;

        try (Connection connection = MainDBPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT id, username, password, full_name " +
                            "FROM users " +
                            "WHERE id=?");
            preparedStatement.setString(1, userId.toString());

            cachedRowSet = CommandDB.select(getSQL(preparedStatement), connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return rowSetToModel(cachedRowSet);
    }

    @Override
    public List<UserModel> getAll() throws SQLException {

        CachedRowSet cachedRowSet;

        try (Connection connection = MainDBPool.getConnection()) {
            cachedRowSet = CommandDB.select(
                    "SELECT id, username, password, full_name " +
                            "FROM users", connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return rowSetToList(cachedRowSet);
    }

    @Override
    public int add(UserModel user) throws SQLException {

        try (Connection connection = MainDBPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO users " +
                            "(id, username, password, full_name) " +
                            "VALUES (?, ?, ?, ?)");
            preparedStatement.setString(1, user.getId().toString());
            preparedStatement.setString(2, user.getUsername());
            preparedStatement.setString(3, user.getPassword());
            preparedStatement.setString(4, user.getFullName());

            status = CommandDB.manipulate(getSQL(preparedStatement), connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return status;
    }

    @Override
    public int update(UserModel user) throws SQLException {

        try (Connection connection = MainDBPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "UPDATE users " +
                            "SET username=?, password=?, full_name=? " +
                            "WHERE id=?");
            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getFullName());
            preparedStatement.setString(4, user.getId().toString());

            status = CommandDB.manipulate(getSQL(preparedStatement), connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return status;
    }

    @Override
    public int delete(UUID userId) throws SQLException {

        try (Connection connection = MainDBPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "DELETE FROM users " +
                            "WHERE id=?");
            preparedStatement.setString(1, userId.toString());

            status = CommandDB.manipulate(getSQL(preparedStatement), connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return status;
    }
    //endregion

    // Additional Methods
    //region
    public List<UserModel> getByProject(UUID projectId) throws SQLException {

        CachedRowSet cachedRowSet;

        try (Connection connection = MainDBPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT users.id, users.username, users.password, users.full_name " +
                            "FROM users JOIN project_users " +
                            "ON users.id = project_users.user_id " +
                            "WHERE project_users.project_id=?");
            preparedStatement.setString(1, projectId.toString());

            cachedRowSet = CommandDB.select(getSQL(preparedStatement), connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return rowSetToList(cachedRowSet);
    }
    //endregion

    // Additional Methods
    //region
    public Optional<UserModel> getByName(String username) throws SQLException {

        CachedRowSet cachedRowSet;

        try (Connection connection = MainDBPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT id, username, password, full_name " +
                            "FROM users " +
                            "WHERE username=?");
            preparedStatement.setString(1, username);

            cachedRowSet = CommandDB.select(getSQL(preparedStatement), connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return rowSetToModel(cachedRowSet);
    }
    //endregion

    // Helper
    //region
    @Override
    protected UserModel mapToObject(CachedRowSet cachedRowSet) throws SQLException {

        UserModel user = new UserModel();

        user.setId(UUID.fromString(cachedRowSet.getString("id")));
        user.setUsername(cachedRowSet.getString("username"));
        user.setPassword(cachedRowSet.getString("password"));
        user.setFullName(cachedRowSet.getString("full_name"));

        return user;
    }
    //endregion
}
