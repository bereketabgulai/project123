package Planner.Classes.Database.DataAccessObjects;

import Planner.Classes.Database.CommandDB;
import Planner.Classes.Database.Pools.MainDBPool;
import Planner.Models.ProjectModel;

import javax.sql.rowset.CachedRowSet;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class ProjectDAO extends DAO<ProjectModel> {
    private int status;

    // CRUD Methods
    //region
    @Override
    public Optional<ProjectModel> get(UUID projectId) throws SQLException {

        CachedRowSet cachedRowSet;

        try (Connection connection = MainDBPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT id, name, description " +
                            "FROM projects " +
                            "WHERE id=?");
            preparedStatement.setString(1, projectId.toString());

            cachedRowSet = CommandDB.select(getSQL(preparedStatement), connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return rowSetToModel(cachedRowSet);
    }

    @Override
    public List<ProjectModel> getAll() throws SQLException {

        CachedRowSet cachedRowSet;

        try (Connection connection = MainDBPool.getConnection()) {
            cachedRowSet = CommandDB.select(
                    "SELECT id, name, description " +
                            "FROM projects", connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return rowSetToList(cachedRowSet);
    }

    @Override
    public int add(ProjectModel project) throws SQLException {

        try (Connection connection = MainDBPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO projects " +
                            "(id, name, description) " +
                            "VALUES (?, ?, ?)");
            preparedStatement.setString(1, project.getId().toString());
            preparedStatement.setString(2, project.getName());
            preparedStatement.setString(3, project.getDescription());

            status = CommandDB.manipulate(getSQL(preparedStatement), connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return status;
    }

    @Override
    public int update(ProjectModel project) throws SQLException {

        try (Connection connection = MainDBPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "UPDATE projects " +
                            "SET name=?, description=? " +
                            "WHERE id=?");
            preparedStatement.setString(1, project.getName());
            preparedStatement.setString(2, project.getDescription());
            preparedStatement.setString(3, project.getId().toString());

            status = CommandDB.manipulate(getSQL(preparedStatement), connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return status;
    }

    @Override
    public int delete(UUID projectId) throws SQLException {

        try (Connection connection = MainDBPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "DELETE FROM projects " +
                            "WHERE id=?");
            preparedStatement.setString(1, projectId.toString());

            status = CommandDB.manipulate(getSQL(preparedStatement), connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return status;
    }
    //endregion

    // Additional Methods
    //region
    public List<ProjectModel> getByUser(UUID userId) throws SQLException {

        CachedRowSet cachedRowSet;

        try (Connection connection = MainDBPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT projects.id, projects.name, projects.description " +
                            "FROM projects JOIN project_users " +
                            "ON projects.id = project_users.project_id " +
                            "WHERE project_users.user_id=?");
            preparedStatement.setString(1, userId.toString());

            cachedRowSet = CommandDB.select(getSQL(preparedStatement), connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return rowSetToList(cachedRowSet);
    }

    public int addUser(UUID projectId, UUID userId) throws SQLException {

        try (Connection connection = MainDBPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO project_users " +
                            "(project_id, user_id) " +
                            "VALUES (?, ?)");
            preparedStatement.setString(1, projectId.toString());
            preparedStatement.setString(2, userId.toString());

            status = CommandDB.manipulate(getSQL(preparedStatement), connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return status;
    }

    public int deleteUser(UUID projectId, UUID userId) throws SQLException {

        try (Connection connection = MainDBPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "DELETE FROM project_users " +
                            "WHERE project_id=? " +
                            "AND user_id=?");
            preparedStatement.setString(1, projectId.toString());
            preparedStatement.setString(2, userId.toString());

            status = CommandDB.manipulate(getSQL(preparedStatement), connection);
        } catch (Exception e) {
            // Exception handling

            throw new SQLException();
        }

        return status;
    }
    //endregion

    // Helper
    //region
    @Override
    protected ProjectModel mapToObject(CachedRowSet cachedRowSet) throws SQLException {

        ProjectModel project = new ProjectModel();

        project.setId(UUID.fromString(cachedRowSet.getString("id")));
        project.setName(cachedRowSet.getString("name"));
        project.setDescription(cachedRowSet.getString("description"));

        return project;
    }
    //endregion
}
