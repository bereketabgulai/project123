package Planner.Models;

import org.apache.commons.codec.binary.Hex;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.UUID;

public class SaltModel {

    // attributes
    private UUID id;
    private String salt;
    private UUID userId;

    private byte[] salty = new byte[20];

    public SaltModel() {
    }

    public SaltModel(UUID id, String salt, UUID userId) {
        this.id = id;
        this.salt = salt;
        this.userId = userId;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }


    /**
     * HASH SALT SEPARATELY
     *
     * @param salt2
     * @return
     */
    public String hashSalt(String salt) {
        try {

            PBEKeySpec spec = new PBEKeySpec(salt.toCharArray(), this.salty, 65536, 256);
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");

            byte[] hash = factory.generateSecret(spec).getEncoded();

            this.salt = Hex.encodeHexString(hash);


        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            System.err.println(e.getMessage());
        }
        return this.salt;
    }

    @Override
    public String toString() {
        return id + " " + salt + " " + userId + "\n";
    }
}
