package Planner.Models;


import java.util.UUID;


/**
 * For dealing with users and their credentials
 */
public class UserModel {

    // private fields
    private UUID id;
    private String username;
    private String fullName;
    private String password;
    private String salt;


    // Getters + Setters

    /**
     * @return the username
     */
    public String getUsername() {
        try {
            return username;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return null;
    }

    /**
     * @param username the username
     */
    public void setUsername(String username) {
        try {
            this.username = username;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * @return the password
     */
    public String getPassword() {
        try {
            return password;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return null;
    }

    /**
     * @param password the password
     */
    public void setPassword(String password) {
        try {
            this.password = password;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * @return the full name
     */
    public String getFullName() {
        try {
            return fullName;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return null;
    }

    /**
     * @param fullName the full name
     */
    public void setFullName(String fullName) {
        try {
            this.fullName = fullName;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * @return the UUID
     */
    public UUID getId() {
        try {
            return id;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return null;
    }

    /**
     * @param id the UUID
     */
    public void setId(UUID id) {
        try {
            this.id = id;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * @return the salt
     */
    public String getSalt() {
        try {
            return salt;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return null;
    }

    /**
     * @param salt the UUID
     */
    public void setSalt(String salt) {
        try {
            this.salt = salt;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    @Override
    public String toString() {
        return getId() + " " + getUsername() + " " + getPassword() + "\n";
    }
}
