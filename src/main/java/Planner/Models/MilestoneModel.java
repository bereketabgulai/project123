package Planner.Models;

import java.util.Date;
import java.util.UUID;

/**
 * Milestone Model implements a single milestone
 */
public class MilestoneModel {

    // private fields
    private UUID id;
    private String name;
    private String description;
    private Date dueDate;
    private Date dateCompleted;
    private UUID projectId;
    private UUID userId;
    // Only populated on run time
    private boolean shared;

    public MilestoneModel() {
    }

    /**
     * Constructor
     *
     * @param description for milestone
     * @param dueDate     date the milestone 'should' be completed
     */
    public MilestoneModel(String description, Date dueDate) {
        this.description = description;
        this.dueDate = dueDate;

        this.id = UUID.randomUUID();
        this.dateCompleted = dueDate;
    }

    public MilestoneModel(String name, String description, Date dueDate) {
        this.name = name;
        this.description = description;
        this.dueDate = dueDate;

        this.id = UUID.randomUUID();
        this.dateCompleted = dueDate;
    }

    // Getters + Setters

    /**
     * @return id for milestone
     */
    public UUID getId() {
        try {
            return id;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return null;
    }

    /**
     * @param id for milestone
     */
    public void setId(UUID id) {
        try {
            this.id = id;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * @return actual date completed for milestone
     */
    public String getName() {
        try {
            return name;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return null;
    }

    /**
     * @param name for milestone
     */
    public void setName(String name) {
        try {
            this.name = name;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * @return description for milestone
     */
    public String getDescription() {
        try {
            return description;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return null;
    }

    /**
     * @param description for milestone
     */
    public void setDescription(String description) {
        try {
            this.description = description;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * @return due date for milestone
     */
    public Date getDueDate() {
        try {
            return dueDate;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return null;
    }

    /**
     * @param dueDate for milestone
     */
    public void setDueDate(Date dueDate) {
        try {
            this.dueDate = dueDate;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * @return actual date completed for milestone
     */
    public Date getDateCompleted() {
        try {
            return dateCompleted;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return null;
    }

    /**
     * @param dateCompleted for milestone
     */
    public void setDateCompleted(Date dateCompleted) {
        try {
            this.dateCompleted = dateCompleted;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * @return userId for milestone
     */
    public UUID getUserId() {
        try {
            return userId;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return null;
    }

    /**
     * @param userId for milestone
     */
    public void setUserId(UUID userId) {
        try {
            this.userId = userId;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * @return projectId for milestone
     */
    public UUID getProjectId() {
        try {
            return projectId;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return null;
    }

    /**
     * @param projectId for milestone
     */
    public void setProjectId(UUID projectId) {
        try {
            this.projectId = projectId;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public boolean isShared() {
        return shared;
    }

    public void setShared(boolean shared) {
        this.shared = shared;
    }

    @Override
    public String toString() {
        return getName() + " " + getId() + " " + getDescription() + "\n";
    }


}
