package Planner.Models;

import Planner.Classes.Database.DataAccessObjects.MilestoneDAO;
import Planner.Classes.Database.DataAccessObjects.ProjectDAO;
import Planner.Classes.Database.DataAccessObjects.SaltDAO;
import Planner.Classes.Database.DataAccessObjects.UserDAO;
import org.apache.commons.codec.binary.Hex;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.util.*;

/**
 * Allows for building lists of milestones
 */
public class ToDoAppModel {

    //region Private fields
    private MilestoneDAO milestoneDAO = new MilestoneDAO();
    private UserDAO userDAO = new UserDAO();
    private SaltDAO saltDAO = new SaltDAO();
    private ProjectDAO projectDAO = new ProjectDAO();


    // Just putting this here for now. Might be useful for having more than one project
    private UUID uuid;

    private List<MilestoneModel> milestones;

    private List<UserModel> users;

    private List<SaltModel> salts;

    private List<ProjectModel> projects;

    private byte[] salt = new byte[20];

    private String encryptedPassword;
    //endregion

    //region Constructor

    /**
     * Constructor
     */
    public ToDoAppModel() {
        this.milestones = new ArrayList<>();
        this.users = new ArrayList<>();

        // Just putting this here for now. Might be useful for having more than one project
        this.uuid = UUID.randomUUID();

        try {
            getUsers();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    //endregion

    //region Milestones

    /**
     * CREATE MILESTONE
     *
     * @param milestone gets added to list
     */
    @SuppressWarnings("Duplicates")
    public void addMilestone(MilestoneModel milestone) throws SQLException {
        try {

            Optional<MilestoneModel> opt = Optional.ofNullable(milestone);
            if (opt.isPresent()) {
                milestoneDAO.add(milestone);
                milestones.add(milestone);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            throw new SQLException();
        }
    }

    /**
     * READ MILESTONES
     *
     * @return milestones get read in
     */
    @SuppressWarnings("Duplicates")
    public Optional<List<MilestoneModel>> getMilestones() throws SQLException {

        Optional<List<MilestoneModel>> opt = Optional.ofNullable(milestoneDAO.getAll());
        try {
            if (opt.isPresent()) {
                milestones = milestoneDAO.getAll();
                return opt;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            throw new SQLException();
        }
        return opt;
    }

    /**
     * READ MILESTONE
     *
     * @param uuid id for milestone
     * @return milestone
     */
    @SuppressWarnings("Duplicates")
    public MilestoneModel getMilestone(UUID uuid) throws SQLException {
        try {
            Optional<MilestoneModel> opt = milestoneDAO.get(uuid);

            if (opt.isPresent()) {
                // TODO: indexOf when it is empty causes out-of-bound error
//                int i = milestones.indexOf(uuid);
                return opt.get();
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            throw new SQLException();
        }
        return null;
    }

    /**
     * UPDATE MILESTONE
     *
     * @param milestone gets updated
     * @throws SQLException
     */
    @SuppressWarnings("Duplicates")
    public void updateMilestone(MilestoneModel milestone) throws SQLException {
        try {
            Optional<MilestoneModel> opt = Optional.ofNullable(milestone);

            milestones = milestoneDAO.getAll();
            if (opt.isPresent()) {
                int i = Arrays.asList(milestone).indexOf(milestone);
                milestones.set(i, milestone);
                milestoneDAO.update(milestone);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.out.println(milestones.toString());
            throw new SQLException();

        }
    }

    /**
     * DELETE MILESTONE
     *
     * @param milestone gets removed
     */
    @SuppressWarnings("Duplicates")
    public void removeMilestone(MilestoneModel milestone) {
        try {
            Optional<MilestoneModel> opt = Optional.ofNullable(milestone);
            if (opt.isPresent()) {
                milestoneDAO.delete(milestone.getId());
                milestones.remove(milestone);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    //endregion

    //region Users

    /**
     * CREATE USER
     *
     * @param user to be added
     */
    public void addUsers(UserModel user) {
        try {
            Optional<UserModel> opt = Optional.ofNullable(user);

            if (opt.isPresent()) {
                userDAO.add(user);
                users.add(user);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * READ USERS
     *
     * @return all users
     */
    public List<UserModel> getUsers() throws SQLException {
        try {
            users = userDAO.getAll();
            return users;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            throw new SQLException();
        }
    }

    /**
     * READ USER BY NAME
     *
     * @param name username
     * @return Optional.ofNullable(user)
     * @throws SQLException
     */
    public Optional<UserModel> getUserByName(String name) throws SQLException {

        // Optional <UserModel> opt = userDAO.getByName(name);

        for (UserModel user : users) {
            if (user.getUsername().equals(name)) {
                return Optional.ofNullable(user);
            }
        }
        return Optional.empty();
    }

    /**
     * UPDATE USER
     *
     * @param user
     * @throws SQLException
     */
    public void updateUser(UserModel user) throws SQLException {
        try {
            userDAO.update(user);

            // userDAO = userDAO.getAll();

            int i = Arrays.asList(user).indexOf(user);

            users.set(i, user);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            throw new SQLException();
        }
    }

    /**
     * DELETE USER
     *
     * @param user
     */
    public void removeUser(UserModel user) {
        try {
            userDAO.delete(user.getId());
            users.remove(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    //endregion

    //region Salts

    /**
     * CREATE SALT
     *
     * @param salt
     * @throws SQLException
     */
    @SuppressWarnings("Duplicates")
    public void addSalt(SaltModel salt) throws SQLException {
        try {
            Optional<SaltModel> opt = Optional.ofNullable(salt);

            if (opt.isPresent()) {
                saltDAO.add(salt);
                salts.add(salt);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            throw new SQLException();
        }
    }

    /**
     * READ SALTS
     *
     * @return
     * @throws SQLException
     */
    @SuppressWarnings("Duplicates")
    public List<SaltModel> getSalts() throws SQLException {
        try {
            Optional<List<SaltModel>> opt = Optional.ofNullable(saltDAO.getAll());
            if (opt.isPresent()) {
                salts = saltDAO.getAll();
                return salts;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            throw new SQLException();
        }
        return null;
    }

    /**
     * UPDATE SALT
     *
     * @param salt
     * @throws SQLException
     */
    public void updateSalt(SaltModel salt) throws SQLException {
        try {
            Optional<SaltModel> opt = Optional.ofNullable(salt);

            if (opt.isPresent()) {
                saltDAO.update(salt);

                int i = Arrays.asList(salt).indexOf(salt);
                saltDAO.update(salt);
                salts.set(i, salt);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            throw new SQLException();
        }
    }

    /**
     * DELETE SALT
     *
     * @param salt
     * @throws SQLException
     */
    public void removeSalt(SaltModel salt) throws SQLException {
        try {
            Optional<SaltModel> opt = Optional.ofNullable(salt);
            if (opt.isPresent()) {
                saltDAO.delete(salt.getId());
                salts.remove(salt);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            throw new SQLException();
        }
    }
    // endregion

    //region Projects

    /**
     * CREATE PROJECT
     *
     * @param project
     * @throws SQLException
     */
    @SuppressWarnings("Duplicates")
    public void addProject(ProjectModel project) throws SQLException {
        try {
            Optional<ProjectModel> opt = Optional.ofNullable(project);

            if (opt.isPresent()) {
                projectDAO.add(project);
                projects.add(project);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            throw new SQLException();
        }
    }

    /**
     * READ PROJECTS
     *
     * @return
     * @throws SQLException
     */
    public List<ProjectModel> getProjects() throws SQLException {
        try {
            Optional<List<ProjectModel>> opt = Optional.ofNullable(projects);

            if (opt.isPresent()) {
                projects = projectDAO.getAll();
                return projects;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            throw new SQLException();
        }
        return null;
    }

    /**
     * READ PROJECT
     *
     * @param uuid
     * @return
     * @throws SQLException
     */
    @SuppressWarnings("Duplicates")
    public ProjectModel getProject(UUID uuid) throws SQLException {
        try {
            Optional<ProjectModel> opt = projectDAO.get(uuid);

            if (opt.isPresent()) {
                int i = projects.indexOf(uuid);
                return projects.get(i);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            throw new SQLException();
        }
        return null;
    }

    /**
     * UPDATE PROJECT
     *
     * @param project
     * @throws SQLException
     */
    @SuppressWarnings("Duplicates")
    public void updateProject(ProjectModel project) throws SQLException {
        try {
            Optional<ProjectModel> opt = Optional.ofNullable(project);


            if (opt.isPresent()) {
                int i = Arrays.asList(project).indexOf(project);
                projects.set(i, project);
                projectDAO.update(project);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            throw new SQLException();
        }
    }

    /**
     * DELETE PROJECTS
     *
     * @param project
     */
    @SuppressWarnings("Duplicates")
    public void removeProject(ProjectModel project) {
        try {
            Optional<ProjectModel> opt = Optional.ofNullable(project);
            if (opt.isPresent()) {
                projectDAO.delete(project.getId());
                projects.remove(project);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
//endregion

    //region Salt+Pass Encryptor

    /**
     * HASH BOTH SALT AND PASSWORD TOGETHER
     *
     * @param salt
     * @return
     */
    public String encryptSaltPassword(String Password, String salt) {
        try {

            byte[] salty = salt.getBytes();

            PBEKeySpec spec = new PBEKeySpec(salt.toCharArray(), salty, 65536, 256);
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");

            byte[] hash = factory.generateSecret(spec).getEncoded();

            this.encryptedPassword = Hex.encodeHexString(hash);


        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            System.err.println(e.getMessage());
        }
        return this.encryptedPassword;
    }
    //endregion

}
