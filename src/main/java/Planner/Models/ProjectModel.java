package Planner.Models;

import java.util.List;
import java.util.UUID;

public class ProjectModel {

    private UUID id;
    private String name;
    private String description;
    private List<MilestoneModel> milestones;
    private List<UserModel> users;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<MilestoneModel> getMilestones() {
        return milestones;
    }

    public void setMilestones(List<MilestoneModel> milestones) {
        this.milestones = milestones;
    }

    public List<UserModel> getUsers() {
        return users;
    }

    public void setUsers(List<UserModel> users) {
        this.users = users;
    }
}
