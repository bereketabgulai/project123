// Driver Function, waits for DOM build
$(document).ready(function () {
    formatDueDates();

    buttonHandling();
});

// Initialises listeners for all buttons in the page
function buttonHandling() {
    $("#addMilestoneBtn").on("click", function () {
        addMilestone();
    });

    $('.milestoneBtn').on("click", function () {
        handleButtonClick($(this));
    });
}

// Displays quick notification
function toast(type, title) {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top',
        showConfirmButton: false,
        timer: 3000
    });

    // Types: warning|success|info|
    Toast.fire({
        type: type,
        title: title
    })
}

// Adds a milestone
function addMilestone() {
    $("#addForm").submit(function (e) {

        // Avoid to execute the actual submit of the form.
        e.preventDefault();
        // Avoid submit twice
        e.stopImmediatePropagation();

        $.ajax
        ({
            type: "POST",
            data: 'milestone-name=' + $('#milestone-name').val() + '&dateDue=' + $('#dateDue').val(),
            url: "home",
            success: function (content) {
                console.log(content);
                $('#milestones').append(content);
                const milestone = '#' + $(content).attr("id");
                $(milestone).show("slow");
                toast("success", "Added successfully.");

                buttonHandling();
            },
            error: function () {
                toast("error", "Could not add. Error occurred");
            }
        });

        return false;
    });
}

// Redirects the button that was clicked to the corresponding function
function handleButtonClick(buttonClicked) {
    // $(this) refers to button that was clicked
    const id = $(buttonClicked).attr('id');
    // should return edit | share | trash | comp
    const result = id.split("_");
    const uuid = result[0];
    const buttonRole = result[1];


    console.log("Id:", uuid, "Role:", buttonRole);

    switch (buttonRole) {
        case "edit" :
            doEdit(uuid);
            break;
        case  "share" :
            doShare(uuid);
            break;
        case  "trash" :
            doTrash(uuid);
            break;
        case  "comp" :
            doComplete(uuid);
            break;
    }
}

// Enables editing of milestone in conjunction with showEditDialog function
function doEdit(uuid) {
    $.ajax
    ({
        type: "GET",
        data: `milestone-uuid=${uuid}&get-milestone-data=true`,
        url: "home",
        success: function (response_milestone) {
            console.log("Milestone: ", response_milestone);
            // Ensure not empty
            if (response_milestone !== "") {
                // Convert it json object
                response_milestone = JSON.parse(response_milestone);

                showEditDialog(response_milestone.id, response_milestone.name, getISODate(response_milestone.dueDate));
            }
        },
        error: function () {
            toast("error", "Could not edit. Error occurred");
        }
    });
}

// Handles sharing of milestone in conjunction with showShareDialog function
function doShare(uuid) {
    $.ajax
    ({
        type: "GET",
        data: `milestone-uuid=${uuid}&get-milestone-share-uuid=true`,
        url: "/share",
        success: function (response) {
            response = JSON.parse(response);

            console.log("Milestone: ", response.sharedUuid, response.shared);
            showShareDialog(uuid, response.sharedUuid, response.shared);
        },
        error: function () {
            toast("error", "Could not share. Error occurred");
        }
    });
}

// Handles deleting of milestone
function doTrash(uuid) {
    Swal.fire({
        title: "Delete this milestone?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: '<i class="fas fa-trash-alt fa-lg"></i>',
        buttonsStyling: false,
        focusConfirm: false,
        width: "22rem",
        customClass: {
            confirmButton: 'btn btn-primary col btn-dialog-css',
            cancelButton: 'btn btn-default col btn-dialog-css',
            actions: 'actions-class'
        }
    }).then((result) => {
        if (result.value) {
            $.ajax
            ({
                type: "DELETE",
                url: "home" + '?' + $.param({"milestoneId": uuid}),
                success: function (content) {
                    $(`#milestone-${uuid}`).fadeTo(300, 0.01, function () {
                        $(this).slideUp(150, function () {
                            $(this).remove();
                        });
                    });

                    toast("success", "Deleted successfully");
                },
                error: function () {
                    toast("error", "Could not delete. Error occurred");
                }
            });
        }
    });
}

// Handles completing of milestone
function doComplete(uuid) {
    const milestoneName = $(`#m-title-${uuid}`).text();
    const dateDue = $(`#m-date-${uuid}`).text().split(',')[1];

    $.ajax
    ({
        type: "PUT",
        url: "home" + '?' + $.param({"milestone-name": milestoneName})
            + '&' + $.param({"dateDue": getISODate(dateDue)})
            + '&' + $.param({"dateCompleted": Date.now()})
            + '&' + $.param({"milestone-uuid": uuid})
            + '&' + $.param({"fromComplete": "true"}),
        success: function (content) {
            milestone = $('#milestone-' + uuid);
            $(milestone).fadeTo(300, 0.01, function () {
                $(this).slideUp(150, function () {
                    $(this).remove();
                });
            });

            $('#milestonesComplete').append(content);
            toast("success", "Completed successfully.");

            buttonHandling();
        },
        error: function () {
            toast("error", "Could not complete. Error occurred");
        }
    });
}

// Displays the dialog for editing a milestone with given parameters,
// Calls updateMilestone function
function showEditDialog(uuid, milestoneText, milestoneDate) {
    Swal.fire({
        showCancelButton: true,
        titleText: "Edit this milestone?",
        html:
            '  <form>\n' +
            '    <div class="row">\n' +
            '      <div class="col-sm-12"><textarea id="milestone-text' + uuid + '" name="milestone-text' + uuid + '" class="form-control w-40 h-10 mb-2"\n' +
            'placeholder="Enter milestone description here" required>' + milestoneText + '</textarea>' +
            '<input type=\'date\' name="dateDueEdit' + uuid + '" id="dateDueEdit' + uuid + '" class="form-control" min="2019"\n value="'
            + milestoneDate + '" required/>  </div>' + '    </div>' + '  </form> ',
        focusConfirm: false,
        confirmButtonText: '<i class="fas fa-edit fa-lg"></i>',
        buttonsStyling: false,
        focusCancel: true,
        allowEnterKey: false,
        allowOutsideClick: false,
        customClass: {
            confirmButton: 'btn btn-primary col btn-dialog-css',
            cancelButton: 'btn btn-default col btn-dialog-css',
            actions: 'actions-class'
        },
        preConfirm: function () {
            const milestoneId = `#milestone-text${uuid}`;
            const dateId = `#dateDueEdit${uuid}`;
            const milestoneVal = $(milestoneId).val();
            const dateVal = $(dateId).val();
            console.log("aljdfljasdlf", milestoneVal, dateVal);

            if ((milestoneText !== milestoneVal) || (milestoneDate !== dateVal)) {
                updateMilestone(uuid, milestoneVal, dateVal);
            }
        }
    });
}

// Updates a milestone with given parameters
function updateMilestone(uuid, milestoneVal, dateVal) {
    $.ajax
    ({
        type: "PUT",
        url: "home" + '?' + $.param({"milestone-name": milestoneVal}) + '&' + $.param({"dateDue": dateVal})
            + '&' + $.param({"milestone-uuid": uuid}),
        success: function (response_milestone) {
            console.log(response_milestone);
            const title = `#m-title-${uuid}`;
            const date = `#m-date-${uuid}`;

            // Convert it json object
            response_milestone = JSON.parse(response_milestone);


            $(title).text(response_milestone.name);
            $(date).text(response_milestone.dateDue);
            toast("success", "Updated successfully")
        },
        error: function () {
            toast("error", "Could not update. Error occurred");
        }
    });
}

// Displays sharing dialog for the given parameters
function showShareDialog(uuid, milestoneShareUuid, shared) {
    Swal.fire({
        showCancelButton: true,
        titleText: "Share this milestone?",
        html: '<input  type="text" id="shareLinkInput" class="swal2-input" readonly value="http://localhost:9000/share?id=' + milestoneShareUuid + '">',
        focusConfirm: false,
        confirmButtonText: '<i class="fas fa-share-square fa-lg"></i>',
        buttonsStyling: false,
        allowEnterKey: false,
        allowOutsideClick: false,
        customClass: {
            confirmButton: 'btn btn-primary col btn-dialog-css',
            cancelButton: 'btn btn-default col btn-dialog-css',
            actions: 'actions-class'
        },
        preConfirm: function () {
            // Check if it is shared or not
            if (shared === "false") {
                // Send to server for confirmation of sharing
                $.ajax
                ({
                    type: "POST",
                    data: `milestone-uuid=${uuid}&milestone-share-uuid=${milestoneShareUuid}`,
                    url: "share",
                    success: function (response) {
                        console.log(response);

                        $('#shareLinkInput').select();
                        if (document.execCommand('copy')) {
                            toast("success", "Shared successfully. Copied to clipboard.");
                        }
                    },
                    error: function () {
                        toast("error", "Could not update. Error occurred");
                    }
                });
            } else {
                $('#shareLinkInput').select();
                if (document.execCommand('copy')) {
                    toast("success", "Shared successfully. Copied to clipboard.");
                }
            }
        }
    });

}

// Returns formatted date as ddd, dd/mm/yyyy
function formatMSDate(date) {
    return new Date(date).format("ddd, dd/mm/yyyy");
}

// Formats all dates in the milestone list
function formatDueDates() {
    $("#milestone-list p").each(function (i, elem) {
        console.log(formatMSDate($(elem).text()));

        $(elem).text(formatMSDate($(elem).text()))
    });
}

// Returns formatted date as ISO standard date format
function getISODate(date) {
    const dueDate = new Date();
    const curDate = dueDate.getDate();
    const month = dueDate.getMonth();
    const year = dueDate.getFullYear();

    // Adds 0 in front for less than 10 numbers
    function addZero(n) {
        return n < 10 ? '0' + n : n
    }

    return year + "-" + addZero(month + 1) + "-" + addZero(curDate);
}




